import config from 'config'

export const fetchResultListData = () => {
  const url = config.server.url;
  return fetch(url).then(response => response.json());
};
