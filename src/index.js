import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import App from './App';
import { addLocaleData } from 'react-intl';
import es from 'react-intl/locale-data/es';
import registerServiceWorker from './registerServiceWorker';

addLocaleData(es);
ReactDOM.render(<App/>, document.getElementById('root'));
registerServiceWorker();
