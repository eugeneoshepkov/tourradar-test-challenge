/**
 * @flow
 */
import * as messages from './messages/';

export const getMessages = locale => messages[locale];


