import React from 'react';

export const DEFAULT_LOCALE = 'en';

const { Provider, Consumer } = React.createContext({
  locale: DEFAULT_LOCALE,
  setLocale: () => {}
});

export {
  Provider as LocaleProvider,
  Consumer as LocaleConsumer
};

