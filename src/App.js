import React, { Component } from 'react';
import Root from './components/Root';
import Countdown from './components/Countdown';
import ResultList from './components/ResultList';
import { LocaleProvider, DEFAULT_LOCALE } from './lang/LocaleContext';
import './App.css';


class App extends Component {

  setLocale = locale => {
    this.setState({ locale });
  };

  state = {
    locale: DEFAULT_LOCALE,
    setLocale: this.setLocale
  };

  render() {
    return (
      <LocaleProvider value={this.state}>
        <Root>
          <Countdown till="2018-09-09 12:34:00"/>
          <ResultList/>
        </Root>
      </LocaleProvider>
    );
  }
}

export default App;
