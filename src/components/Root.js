import React from 'react';
import { IntlProvider } from 'react-intl';
import styled from 'styled-components';
import logo from '../tourradar.png';
import { getMessages } from '../lang';
import { LocaleConsumer } from '../lang/LocaleContext';
import LanguageSelect from './LanguageSelect';
import { Container } from 'reactstrap';
import { Grid, Cell } from 'styled-css-grid';

const RootWrapper = styled.div`
  text-align: center;
  background-color: #f2f2f2;
  min-height: 100vh;
`;


const Root = ({ children }) => (
  <LocaleConsumer>
    {({ locale, setLocale }) => (
      <IntlProvider locale={locale} messages={getMessages(locale)}>
        <RootWrapper>
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo"/>
            <h1 className="App-title">TourRadar Test Case</h1>
          </header>
          <Container>
            <LanguageSelect onChange={setLocale}/>
            {children}
          </Container>
        </RootWrapper>
      </IntlProvider>
    )}
  </LocaleConsumer>
);

export default Root;
