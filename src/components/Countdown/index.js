/**
 * @flow
 */
import React from 'react';
import CountdownWrapper from './CountdownWrapper';
import Counter from '../Counter';

const Countdown = props => (
  <CountdownWrapper render={props => (<Counter {...props} />)} {...props}/>
);

export default Countdown;
