import React from 'react';
import moment from 'moment';

class CountdownWrapper extends React.Component {
  state = {
    seconds: 0,
    minutes: 0,
    hours: 0,
    days: 0,
    months: 0
  };

  componentDidMount() {
    this.timer = setInterval(this.tick, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  tick = () => {
    const endDate = this.props.till;
    const diff = Date.parse(endDate) - Date.parse(new Date());
    let { months, days, hours, minutes, seconds } = this.state;
    if (diff && diff > 0) {
      const duration = moment.duration(diff, 'milliseconds');
      seconds = duration.seconds();
      minutes = duration.minutes();
      hours = duration.hours();
      days = duration.days();
      months = duration.months();
    } else {
      seconds = 0;
    }

    this.setState({
      months,
      days,
      hours,
      minutes,
      seconds
    });
  };

  render() {
    return (
      <div>
        {this.props.render({ ...this.state })}
      </div>
    );
  }
}

export default CountdownWrapper;
