/**
 * @flow
 */
import styled from 'styled-components';

export default styled.p`
      margin: 10px;
      font-size: 40px;
      font-weight: bold;
`;