/**
 * @flow
 */
import styled from 'styled-components';

export default styled.div`
  max-width: ${props => props.enableMonths ? '600px' : '400px'}
  margin: 0 auto;
`