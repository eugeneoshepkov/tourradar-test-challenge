/**
 * @flow
 */
import styled from 'styled-components';

export default styled.div`
    text-align: center;
    border: 1px solid #777;
    text-transform: uppercase;
    background: #fff;
    padding: 0;
`