import React from 'react';
import CounterContainer from './CounterContainer';
import { FormattedMessage } from 'react-intl';
import TimerContainer from './TimerContainer';
import Timer from './Timer';
import { Grid, Cell } from 'styled-css-grid';
import styled from 'styled-components';

const Title = styled.h4`
  text-align: left;
  text-transform: uppercase;
  font-size: 12px;
`;
class Counter extends React.Component {

  render() {
    const { seconds, minutes, hours, days, months } = this.props;
    const cellSize = months ? 2 : 3;
    return (
      <CounterContainer enableMonths>
        <Title><FormattedMessage id="components.Timer.title"/></Title>
        <Grid columns={12}>
          {
            Boolean(months) && (
              <Cell width={cellSize}>
                <TimerContainer>
                  <Timer> {months}</Timer>
                  <FormattedMessage id="components.Timer.months"/>
                </TimerContainer>
              </Cell>
            )
          }
          <Cell width={cellSize}>
            <TimerContainer>
              <Timer> {days}</Timer>
              <FormattedMessage id="components.Timer.days"/>
            </TimerContainer>
          </Cell>
          <Cell width={cellSize}>
            <TimerContainer>
              <Timer>{hours}</Timer>
              <FormattedMessage id="components.Timer.hours"/>
            </TimerContainer>
          </Cell>
          <Cell width={cellSize}>
            <TimerContainer>
              <Timer>{minutes}</Timer>
              <FormattedMessage id="components.Timer.minutes"/>
            </TimerContainer>
          </Cell>
          <Cell width={cellSize}>
            <TimerContainer>
              <Timer> {seconds}</Timer>
              <FormattedMessage id="components.Timer.seconds"/>
            </TimerContainer>
          </Cell>
        </Grid>
      </CounterContainer>
    );
  }
}

export default Counter;
