import React, { Fragment } from 'react';

const options = [
  { value: 'en', label: 'English' },
  { value: 'es', label: 'Español' },
];

const LanguageOptions = () => (
  <Fragment>
    {options.map(option => (
      <option
        key={option.value}
        value={option.value}
      >
        {option.label}
      </option>
    ))}
  </Fragment>
);

export default LanguageOptions;
