import React from 'react';
import styled from 'styled-components';
import LanguageOptions from './LanguageOptions';
import { withHandlers } from 'recompose';
import { Input } from 'reactstrap';

const InputStyled = styled(Input)`
  margin: 10px;
`;

const LanguageSelect = ({ onChange, value }) => {
  return (
    <InputStyled type="select" name="language" onChange={onChange} value={value}>
      <LanguageOptions/>
    </InputStyled>
  );
};

const enhance = withHandlers({
  onChange: ({ onChange }) => e => onChange(e.target.value)
});

export default enhance(LanguageSelect);
