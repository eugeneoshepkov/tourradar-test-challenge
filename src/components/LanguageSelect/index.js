import React from 'react';
import styled from 'styled-components';
import LanguageSelect from './LanguageSelect';
import { Grid, Cell } from 'styled-css-grid';

const GridContainer = styled(Grid)`
  @media (max-width: 767px) {
    grid-template-columns: 20% auto;
  }
`;

const LanguageControl = ({ onChange }) => {
  return (
    <GridContainer columns={6}>
      <Cell width={1} left={6}>
        <LanguageSelect onChange={onChange}/>
      </Cell>
    </GridContainer>
  );
};

export default LanguageControl;
