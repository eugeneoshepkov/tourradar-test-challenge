import React, { Fragment } from 'react';
import { defineMessages, injectIntl } from 'react-intl';

const messages = defineMessages({
  'price-asc': {
    id: "components.SortingOptions.price"
  },
  'price-desc': {
    id: "components.SortingOptions.priceDesc"
  },
  'length-desc': {
    id: "components.SortingOptions.lengthDesc"
  },
  'length-asc': {
    id: "components.SortingOptions.length"
  },
});
const options = [
  {
    value: "price-asc"
  },
  {
    value: "price-desc"
  },
  {
    value: "length-desc"
  },
  {
    value: "length-asc"
  }
];

const SortingOptions = ({ intl: { formatMessage } }) => (
  <Fragment>
    {options.map(option => (
      <option
        key={option.value}
        value={option.value}
      >
        {formatMessage(messages[option.value])}
      </option>
    ))}
  </Fragment>
);

export default injectIntl(SortingOptions);
