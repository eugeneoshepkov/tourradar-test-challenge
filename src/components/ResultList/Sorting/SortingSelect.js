import React from 'react';
import SortingOptions from './SortingOptions';
import { Input } from 'reactstrap';
import { withHandlers } from 'recompose';


const Sorting = ({ onChange, value }) => {
  return (
    <Input
      type="select"
      name="sorting"
      onChange={onChange}
      value={value}
      defaultValue="price"
    >
      <SortingOptions/>
    </Input>
  );
};

const enhance = withHandlers({
  onChange: ({ onChange }) => e => onChange(e.target.value)
});

export default enhance(Sorting);
