import React from 'react';
import SortingSelect from './SortingSelect';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { Grid, Cell } from 'styled-css-grid';

const LabelCell = styled(Cell)`
  justify-self: end;
  font-color: #6d6c6c;
`;

const GridContainer = styled(Grid)`
  @media (max-width: 767px) {
    grid-template-columns: 20% auto;
  }
`;

const Sorting = (props) => {
  return (
    <GridContainer columns={6}>
      <LabelCell middle left={5}>
        <FormattedMessage id="components.Sorting.label"/>
      </LabelCell>
      <Cell left={6}>
        <SortingSelect {...props}/>
      </Cell>
    </GridContainer>
  );
};


export default Sorting;
