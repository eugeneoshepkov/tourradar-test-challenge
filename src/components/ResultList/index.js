import React from 'react';
import ResultList from './ResultList';
import styled from 'styled-components';
import { fetchResultListData } from '../../api';
import Spinner from '../Spinner';

const ResultListWrapper = styled.div`
  margin-top: 2rem;
`;

class ResultListContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      isLoading: false
    };
  }

  handleSortingChange = value => {
    const { data } = this.state;
    const sorted = this.sortData(value, [ ...data ]);

    this.setState({
      data: sorted
    });
  };


  sortData(type, data) {
    const field = type.split('-')[0];
    const order = type.split('-')[1];
    let sorted;
    if (order === "asc") {
      sorted = [...data].sort((a, b) => a[field] - b[field]);
    } else {
      sorted = [...data].sort((a, b) => b[field] - a[field]);
    }

    return sorted;
  }

  componentDidMount() {
    this.setState({
      isLoading: true
    });
    fetchResultListData()
      .then(data => this.sortData('price-asc', data))
      .then(
        data => (
          this.setState({
            data,
            isLoading: false
          })
        )
      ).catch(error => {
        console.error(error);
        this.setState({
          isLoading: false
        });
      }
    );
  }

  render() {
    const { data, isLoading } = this.state;
    return (
      <ResultListWrapper>
        {isLoading && <Spinner/>}
        {Boolean(data.length) && <ResultList data={data} onSortingChange={this.handleSortingChange}/>}
      </ResultListWrapper>
    );
  }
}

export default ResultListContainer;
