import React from 'react';
import styled from 'styled-components';
import { Grid, Cell } from 'styled-css-grid';
import ItemInfo from './ItemInfo';
import PriceInfo from './PriceInfo';

const Image = styled.img`
  width: 100%;
`;

const MapImageCell = styled(Cell)`
  height: initial;
  align-self: end;
`;

const Description = styled(Cell)`
  text-align: left;
  margin-top: 10px;
`;

const Price = styled(Cell)`
  margin-top: 10px;
  padding-right: 25px;
`;

const PackageWrapper = styled.div`
  background-color: white;
  margin: 10px 0;
`;


const PackageItem = ({ item }) => {
  return (
    <PackageWrapper>
      <Grid flow="column" columns={3} gap="14px">
        <Cell height={1}>
          <Image src={item.tour_image}/>
        </Cell>
        <MapImageCell height={2}>
          <Image src={item.map_image}/>
        </MapImageCell>
        <Description height={2}>
          <h4>
            {item.tour_name}
          </h4>
          {item.description}
          <ItemInfo item={item}/>
        </Description>
        <Price height={3}>
          <PriceInfo item={item}/>
        </Price>
      </Grid>
    </PackageWrapper>
  );
};

export default PackageItem;
