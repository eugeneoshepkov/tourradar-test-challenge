/**
 * @flow
 */
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Row, Col } from 'reactstrap';
import styled from 'styled-components';

const InfoContainer = styled.div`
  margin-top: 18px;
  font-size: 13px;
`;

const TitleCell = styled(Col)`
  text-transform: uppercase;
  color: #848484;
  margin: 5px 0;
`;

const InfoCell = styled(Col)`
  margin: 5px 0;
`;

const Title = props => (
  <TitleCell md="5" {...props}/>
);

const Info = props => (
  <InfoCell md="7" {...props}/>
);

const TextDestinations = styled.p`
  @media (max-width: 767px) {
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  }
`;


const ItemInfo = ({ item }) => {
  return (
    <InfoContainer>
      <Row>
        <Title>
          <FormattedMessage id="components.PackageItem.destinations" />
        </Title>
        <Info>
          <TextDestinations>{item.destinations.join(', ')}</TextDestinations>
        </Info>
      </Row>
      <Row>
        <Title>
          <FormattedMessage id="components.PackageItem.startsEndsIn" />
        </Title>
        <Info>
          {`${item.destinations[0]} / ${item.destinations[item.destinations.length - 1]}`}
        </Info>
      </Row>
      <Row>
        <Title>
          <FormattedMessage id="components.PackageItem.ageRange" />
        </Title>
        <Info>
          {`${item.age_from} to ${item.age_to}`} years old
        </Info>
      </Row>
      <Row>
        <Title>
          <FormattedMessage id="components.PackageItem.country" />
        </Title>
        <Info>
          {`${item.country}`}
        </Info>
      </Row>
      <Row>
        <Title>
          <FormattedMessage id="components.PackageItem.operator" />
        </Title>
        <Info>
          {`${item.tour_operator}`}
        </Info>
      </Row>
    </InfoContainer>
  );
};

export default ItemInfo;
