import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { Grid, Cell } from 'styled-css-grid';

const CellStyled = styled(Cell)`
  justify-self: left;
`;

const CellBorder = styled(Cell)`
  border-top: 1px solid #eee;
  border-bottom: 1px solid #eee;
  padding: 7px;
`;

const PriceInfo = ({ item }) => {
  return (
    <Grid columns={3} justifyContent="left">
      <CellStyled>
        <FormattedMessage id="components.PriceInfo.saving"/>
      </CellStyled>
      <CellStyled left={3}>
        <FormattedMessage id="components.PriceInfo.from"/>
      </CellStyled>
      <CellStyled>
        <strong>{`€ ${item.saving.toLocaleString()}`}</strong>
      </CellStyled>
      <CellStyled left={3}>
        <strong>{`€ ${item.price.toLocaleString()}`}</strong>
      </CellStyled>
      <CellBorder width={3}>
        <FormattedMessage id="components.PriceInfo.days" values={{ count: item.length }}/>
      </CellBorder>
    </Grid>
  );
};

export default PriceInfo;
