import React from 'react';
import LazyLoad from 'react-lazyload';
import PackageItem from './PackageItem';
import Sorting from './Sorting';

const ResultList = (props) => {
  const { data } = props;
  return (
    <div>
      <Sorting onChange={props.onSortingChange}/>
      {data.map((item, index) => (
        <LazyLoad height={300} offset={100}>
          <PackageItem item={item} key={index}/>
        </LazyLoad>
      ))}
    </div>
  );
};


export default ResultList;
