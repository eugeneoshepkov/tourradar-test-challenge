## TourRadar Test Case

*Built with create-react-app*

Demo: https://tourradar-test-challenge.herokuapp.com/
### Install:

- yarn
- yarn start
- Proceed to http://localhost:3000


####  Some of used tools / libraries:
- reactstrap
- node-config
- recompose
- react-lazyload
- styled-components
- styled-css-grid
